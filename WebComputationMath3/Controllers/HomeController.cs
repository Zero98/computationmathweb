﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebComputationMath3.Models;
using WebComputationMath3.Services.Functions;
using WebComputationMath3.ViewModels;

namespace WebComputationMath3.Controllers
{
    public class HomeController : Controller
    {
        private double[] args;
        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Privacy()
        {
            return View();
        }
        
        [HttpGet]
        public IActionResult Interpolation()
        {
            var args = "1 2 3 4 5 6";
            var function = "sin(x)";
            
            var dataPoints = FunctionManager.ComputeValuesByArgs(function, args);
            InterpolationViewModel interpolationViewModel = new InterpolationViewModel(dataPoints, FunctionManager.GetFunctionByName(function))
                .SetNewtonInterpolation();

            var interpolationValue = interpolationViewModel.Execute();
            
            if (interpolationValue.Count > 0)
                ViewBag.DataPoints = JsonConvert.SerializeObject(interpolationViewModel.GetFunctionValues());
            else
                ViewBag.DataPoints = JsonConvert.SerializeObject(new List<DataPoint>());
            
            ViewBag.Interpolation = JsonConvert.SerializeObject(interpolationValue);
            ViewBag.Args = interpolationViewModel.DataPoints;
            ViewBag.Status = interpolationViewModel.StatusString;
            ViewBag.FuncList = FunctionManager.GetFunctionList();
            
            return View();
        }
        
        [HttpPost]
        public IActionResult Interpolation(string function, string args)
        {
            var dataPoints = FunctionManager.ComputeValuesByArgs(function, args);
            InterpolationViewModel interpolationViewModel = new InterpolationViewModel(dataPoints, FunctionManager.GetFunctionByName(function))
                .SetNewtonInterpolation();
            
            var interpolationValue = interpolationViewModel.Execute();

            if (interpolationValue.Count > 0)
                ViewBag.DataPoints = JsonConvert.SerializeObject(interpolationViewModel.GetFunctionValues());
            else
                ViewBag.DataPoints = JsonConvert.SerializeObject(new List<DataPoint>());
            
            ViewBag.Interpolation = JsonConvert.SerializeObject(interpolationValue);
            ViewBag.Args = interpolationViewModel.DataPoints;
            ViewBag.Status = interpolationViewModel.StatusString;
            
            ViewBag.FuncList = FunctionManager.GetFunctionList();
            
            return View();
        }

        [HttpGet]
        public IActionResult SolveDifferentialEquation()
        {
            var function = "x^3 - 2 * y";
            var point = new DataPoint(0, 0);
            var accuracy = 0.1;
            var rightLimit = 4;
            
            var differentialEquationVM = new DifferentialEquationViewModel(function)
                .SetNewtonInterpolation()
                .SetMilneDifferentialEquation();

            ViewBag.DifferentialEquationValue = JsonConvert.SerializeObject(differentialEquationVM.TryExecute(
                point, accuracy, rightLimit));
            
            ViewBag.Status = differentialEquationVM.StatusString;
            
            ViewBag.CurrentFunction = differentialEquationVM.FunctionName;
            ViewBag.FuncList = differentialEquationVM.GetFunctionList();

            ViewBag.Accuracy = accuracy;
            ViewBag.RightLimit = rightLimit;
            ViewBag.X0 = point.X;
            ViewBag.Y0 = point.Y;
                
            return View();
        }
        
        [HttpPost]
        public IActionResult SolveDifferentialEquation(string function, 
            double x0, double y0, double accuracy, double rightLimit)
        {
            var differentialEquationVM = new DifferentialEquationViewModel(function)
                .SetNewtonInterpolation()
                .SetMilneDifferentialEquation();

            ViewBag.DifferentialEquationValue = JsonConvert.SerializeObject(differentialEquationVM.TryExecute(
                new DataPoint(x0, y0), accuracy, rightLimit));
            
            ViewBag.Status = differentialEquationVM.StatusString;

            ViewBag.CurrentFunction = differentialEquationVM.FunctionName;
            ViewBag.FuncList = differentialEquationVM.GetFunctionList();
            
            ViewBag.Accuracy = accuracy;
            ViewBag.RightLimit = rightLimit;
            ViewBag.X0 = x0;
            ViewBag.Y0 = y0;
            
            return View();
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}