using System;

namespace WebComputationMath3.Services.FunctionsTwoVariables
{
    public abstract class AbstractFunction
    {
        protected string name;
        
        public Func<double,double,double> Function { get; protected set; }
        
        public string Name{ get => name;}
        
        public abstract void CheckADV(double x0, double y0);
    }
}