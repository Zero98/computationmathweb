using System;

namespace WebComputationMath3.Services.FunctionsTwoVariables
{
    public class TestFunction1 : AbstractFunction
    {
        public TestFunction1()
        {
            name = "x^3 - 2 * y";
            Function = ComputeFunction;
        }
        
        public override void CheckADV(double x0, double y0)
        {
            
        }

        private double ComputeFunction(double x, double y)
        {
            double result = Math.Pow(x, 3);
            result += -2 * y;
            return result;
        }
    }
}