using System;

namespace WebComputationMath3.Services.FunctionsTwoVariables
{
    public class TestFunction2 : AbstractFunction
    {
        public TestFunction2()
        {
            name = "3 * x^2";
            Function = ComputeFunction;
        }
        
        public override void CheckADV(double x0, double y0)
        {
            
        }

        private double ComputeFunction(double x, double y) => 3 * Math.Pow(x, 2);
    }
}