using System.Collections.Generic;

namespace WebComputationMath3.Services.FunctionsTwoVariables
{
    public static class FunctionManager
    {
        private static Dictionary<string, AbstractFunction> functions = new Dictionary<string, AbstractFunction>()
        {
            {"x^3 - 2 * y", new TestFunction1()},
            {"3 * x^2", new TestFunction2()},
        };   
        
        public static string[] GetFunctionList()
        {
            var namesList = new List<string>();
            foreach (var abstractFunctionKeyPair in functions)
            {
                namesList.Add(abstractFunctionKeyPair.Value.Name);
            }

            return namesList.ToArray();
        }
        
        public static AbstractFunction GetFunctionByName(string functionName) => functions[functionName];
        
    }
}