using System;
using System.Collections.Generic;
using System.Linq;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Functions
{
    public static class FunctionManager
    {
        private static Dictionary<string, AbstractFunction> functions = new Dictionary<string, AbstractFunction>()
        {
            {"sin(x)", new SinFunction()},
            {"x^3 + 3x^2 - x - 1", new TestFunction()},
            {"sqrt(x)", new SecondTestFunction()}
        };

        public static string[] GetFunctionList()
        {
            var namesList = new List<string>();
            foreach (var abstractFunctionKeyPair in functions)
            {
                namesList.Add(abstractFunctionKeyPair.Value.Name);
            }

            return namesList.ToArray();
        }

        public static AbstractFunction GetFunctionByName(string functionName) => functions[functionName];
        
        public static List<DataPoint> ComputeValuesByArgs(string function, string args) 
        {
            return functions[function.ToLower()].TryGetValuesByArgs(ConvertToDoublesArg(args));
        }

        public static double[] ConvertToDoublesArg(string args)
        {
            List<double> argsD = new List<double>();
            
            var requestLine = args.TrimEnd(new[] {'\r','\n',' '});
            
            foreach (var value in requestLine.Split(' ').Where(n => n != " " && n != ""))
            {
                argsD.Add(double.Parse(value));
            }
            argsD.Sort();

            return  argsD.ToArray();
        }
    }
}