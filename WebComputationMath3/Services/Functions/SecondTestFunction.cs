using System;
using System.Collections.Generic;
using WebComputationMath3.CustomExceptions;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Functions
{
    public class SecondTestFunction : AbstractFunction
    {
        
        public SecondTestFunction()
        {
            Function = Math.Sqrt;
            name = "sqrt(x)";
        }

        public override void CheckADV(List<DataPoint> dataPoints)
        {
            if (dataPoints[0].X < 0)
                throw new ADVException();
        }
    }
}