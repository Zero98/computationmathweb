using System;
using System.Collections.Generic;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Functions
{
    public abstract class AbstractFunction
    {
        protected string name;
        public Func<double, double> Function {  get; protected set; }
        
        public string Name{ get => name;}
        
        public virtual List<DataPoint> TryGetValuesByArgs(double[] args)
        {
            var dataPoins = new List<DataPoint>();
            try
            {
                foreach (var arg in args)
                {
                    dataPoins.Add(new DataPoint(arg, Function(arg)));
                }
            }
            catch
            {
                return new List<DataPoint>();
            }
            return dataPoins;    
        }

        public abstract void CheckADV(List<DataPoint> dataPoints);
    }
}