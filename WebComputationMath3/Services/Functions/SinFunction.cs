using System;
using System.Collections.Generic;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Functions
{
    internal class SinFunction : AbstractFunction
    {
        public SinFunction()
        {
            Function = Math.Sin;
            name = "sin(x)";
        }

        public override void CheckADV(List<DataPoint> dataPoints) { }
    }
}