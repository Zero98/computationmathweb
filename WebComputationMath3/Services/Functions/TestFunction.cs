using System;
using System.Collections.Generic;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Functions
{
    public class TestFunction : AbstractFunction
    {
        public TestFunction()
        {
            Function = ComputeFunction;
            name = "x^3 + 3x^2 - x - 1";

        }

        public double ComputeFunction(double x)
        {
            double result = Math.Pow(x, 3);
            result += 3 * Math.Pow(x, 2);
            return result - x - 1;
        }

        public override void CheckADV(List<DataPoint> dataPoints) { }
    }
}