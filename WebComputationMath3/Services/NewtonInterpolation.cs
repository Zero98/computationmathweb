using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using WebComputationMath3.CustomExceptions;
using WebComputationMath3.Models;
using WebComputationMath3.Services.Interfaces;

namespace WebComputationMath3.Services
{
    public class NewtonInterpolation : IInterpolationService
    {
        private List<DataPoint> interpolationValues;
        private List<double> x;
        private List<double> y;

        private List<double> computeJ;
        
        private double step;
        
            public void Execute(List<DataPoint> dataPoints)
        {
            DivideXAndY(dataPoints);
            interpolationValues = new List<DataPoint>();

            if (x.Count >= 2)
            {
                step = x[1] - x[0];
                if (IsCorrect())
                {
                    computeJ = new List<double>();
                    for (int i = 0; i < x.Count; i++)
                    {
                        computeJ.Add(ComputeCj(i));
                    }
                    
                    var argStep = (x[x.Count - 1] - x[0]) / (x.Count * 10);

                    for (double arg = x[0]; arg < x[x.Count - 1]; arg += argStep)
                    {
                        interpolationValues.Add(new DataPoint(arg,ComputePn(arg)));
                    }
                        
                    interpolationValues.Add(new DataPoint(x[x.Count - 1], ComputePn(x[x.Count - 1])));
                }
                else
                {
                    throw new Exception("Неравномерный шаг между аргументами. Введите кратные аргументы!");  
                }    
            }
            else
            {
                throw new Exception("Введите хотя бы два различных аргумента!");
            }
            
        }
         
        public List<DataPoint> GetResult() => interpolationValues;

        private bool IsCorrect()
        {
            var isCorrect = true;
            double delta = 0;
            for (int i = 1; i < x.Count; i++)
            {
                delta = x[i] - x[i - 1];
                isCorrect &= Math.Round(Math.Abs(delta - step), 4) < 0.0001;
            }

            return isCorrect;
        }
        
        private void DivideXAndY(List<DataPoint> dataPoints)
        {
            x = new List<double>();
            y = new List<double>();

            for (int i = 0; i < dataPoints.Count; i++)
            {
                if (!x.Contains(dataPoints[i].X))
                {
                    x.Add(dataPoints[i].X);
                    y.Add(dataPoints[i].Y);    
                }
            }   
        }
        
        private double ComputePn(double arg)
        {
            double result = computeJ[0];
            for (int j = 1; j < x.Count; j++)
            {
                double partialRes = computeJ[j];
                for (int k = 0; k < j; k++)
                    partialRes *= arg - x[k];
                result += partialRes;
            }
            return result;
        }

        private double ComputeCj(int j)
        {
            if (j == 0)
                return y[0];
            double delta = ComputeDelta(j, j);
            return delta / (ComputeFactorial(j) * Math.Pow(step, j));
        }

        private double ComputeDelta(int p, int i)
        {
            if (p == 1)
                return y[i] - y[i - 1];
            return ComputeDelta(p - 1, i) - ComputeDelta(p - 1, i - 1);
        }

        private double ComputeFactorial(double value)
        {
            if (value == 0)
                return 1;
            return value * ComputeFactorial(value - 1);
        }
    }
}