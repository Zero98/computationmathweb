using System;
using System.Collections.Generic;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.Interfaces
{
    public interface IInterpolationService
    {
        void Execute(List<DataPoint> dataPoints);

        List<DataPoint> GetResult();
    }
}