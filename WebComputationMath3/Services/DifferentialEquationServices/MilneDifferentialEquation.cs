using System;
using System.Collections.Generic;
using Newtonsoft.Json.Schema;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.DifferentialEquationServices
{
    public class MilneDifferentialEquation : AbstractDifferentialEquation
    {
        public override List<DataPoint> SolveEquation(DataPoint startPoint, 
            double accuracy, double rightLimit, Func<double,double,double> function)
        {
            
            var isCalculated = false;
            List<double> differentialList;
            List<DataPoint> differentialValues = new List<DataPoint>();
            var pointsCount = STARTPOINTSCOUNT;
            var step = (rightLimit - startPoint.X) / pointsCount;
            
            while (!isCalculated)
            {
                var isReduceStep = false;
                isCalculated = true;
                
                differentialValues = GetByRungeKutta(startPoint, step, function, out differentialList);

                for (int i = 4; i < pointsCount+1; i++)
                {
                    double newX = differentialValues[i - 1].X + step;
                    double yPred = (differentialValues[i - 4].Y + (4 * step / 3) *
                                    (2 * differentialList[i - 3] - differentialList[i - 2] + 2 * differentialList[i - 1]));
                    differentialList.Add(function(newX, yPred));
                    double yCor = (differentialValues[i - 2].Y +
                                   step / 3 * (differentialList[i - 2] + 4 * differentialList[i - 1] +
                                               differentialList[i]));
                    
                    if (Math.Abs(yCor - yPred) / 29 > accuracy )
                    {
                        step /= 2;
                        pointsCount *= 2;
                        isReduceStep = true;
                        break;
                    }
                    else
                    {
                        differentialValues.Add(new DataPoint(newX, yCor));
                        differentialList[i] = function(newX, yCor);
                    }
                }

                if (isReduceStep) isCalculated = false;
            }
            
            return differentialValues;
            }
    }
}