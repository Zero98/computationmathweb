using System;
using System.Collections.Generic;
using WebComputationMath3.Models;

namespace WebComputationMath3.Services.DifferentialEquationServices
{
    public abstract class AbstractDifferentialEquation
    {
        protected const double STARTPOINTSCOUNT = 5;  
        
        public abstract List<DataPoint> SolveEquation(DataPoint startPoint, double accuracy
            , double rightLimit, Func<double, double, double> function);

        public virtual List<DataPoint> GetByRungeKutta(DataPoint startPoint,double step ,
            Func<double, double, double> function,out List<double> differentialList)
        {
            differentialList = new List<double>();
                
            var kuttaСoef = new double[4];
            var startDataPoints = new List<DataPoint>(){startPoint};
            
            for (int i = 0; i < 3; i++)
            {
                kuttaСoef[0] = function(startDataPoints[i].X, startDataPoints[i].Y);
                kuttaСoef[1] = function(startDataPoints[i].X + step / 2, startDataPoints[i].Y + kuttaСoef[0] / 2);
                kuttaСoef[2] = function(startDataPoints[i].X + step / 2, startDataPoints[i].Y + kuttaСoef[1] / 2);
                kuttaСoef[3] = function(startDataPoints[i].X + step, startDataPoints[i].Y + kuttaСoef[2]);
                var newDataPoint = new DataPoint(
                    startDataPoints[i].X + step,
                    (startDataPoints[i].Y +
                     step * (kuttaСoef[0] + 2 * kuttaСoef[1] + 2 * kuttaСoef[2] + kuttaСoef[3]) / 6.0));
                
                startDataPoints.Add(newDataPoint);
                
                differentialList.Add(function(startDataPoints[i].X, startDataPoints[i].Y));
            }
            
            differentialList.Add(function(startDataPoints[3].X, startDataPoints[3].Y));
            
            return startDataPoints;
        }
    }
}