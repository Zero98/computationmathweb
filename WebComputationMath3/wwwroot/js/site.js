﻿function  ValidateInterpolationForm() {
    var argsLine = document.forms["interpolationForm"]["args"].value
    var value = document.getElementById("function-select").value
    if(value === ""){
        SendErrorMsg("Вы не выбрали никакую функцию!")
        return false;
    }
    else if(CountArgs(argsLine) < 2){
        SendErrorMsg("Для интерполяции нужно минимум два элемента!")
        return false;
    }
    else if(CountArgs(argsLine) > 26){
        SendErrorMsg("Пожалуйста, введите меньше аргументов!")
        return false;
    }
    else if(/[^\d\s\,\-]/g.test(argsLine.replace(" ",""))){
        SendErrorMsg("Введите корректные символы!")
        return false;
    }
    
    
    ClearErrorMsg()
    
    return true
}

function ValidateDifferentialForm(){
    var x0 = document.forms["differentialEquationForm"]["X0"].value
    var y0 = document.forms["differentialEquationForm"]["Y0"].value
    var rightLimit = document.forms["differentialEquationForm"]["RightLimit"].value
    var accuracy = document.forms["differentialEquationForm"]["Accuracy"].value
    var value = document.getElementById("function-select").value
    
    if(value === ""){
        SendErrorMsg("Вы не выбрали никакую функцию!")
        return false;
    }


    if(x0 > rightLimit){
        SendErrorMsg("Правая граница должна быть больше X0!")
        return false;
    }
    
    if(ChechDoubleCorrect(x0,"X0") &&
        ChechDoubleCorrect(y0,"Y0") &&
        ChechDoubleCorrect(rightLimit,"конец отрезка") &&
        ChechDoubleCorrect(accuracy,"точность вычислений")){
        
        ClearErrorMsg()
       return true 
    }
    
    return false
}

function ChechDoubleCorrect(arg, fieldName) {
    if(/[^\d\s\,\-]/g.test(arg.replace(" ",""))){
        SendErrorMsg("Введите корректные символы в поле " +fieldName+ "!")
        return false
    }
    return true
}

function ClearErrorMsg(){
    var err =  document.getElementById("#error")
    err.removeChild(err.firstChild);
}

function SendErrorMsg(msg) {
    document.getElementById("error").innerHTML = msg
}

function CountArgs(args) {
    var rex = /(-)?\d+((,)\d+)?/g;
    return args.match(rex).length;
}