using System;
using System.Collections.Generic;
using WebComputationMath3.CustomExceptions;
using WebComputationMath3.Models;
using WebComputationMath3.Services;
using WebComputationMath3.Services.Functions;
using WebComputationMath3.Services.Interfaces;

namespace WebComputationMath3.ViewModels
{
    public class InterpolationViewModel
    {
        private IInterpolationService interpolationService;
        private AbstractFunction function;
        
        private List<DataPoint> dataPoints;
        
        private List<DataPoint> interpolationValues;

        public string StatusString { get; private set; }

        public List<DataPoint> DataPoints
        {
            get => dataPoints;
            set { value = dataPoints; }
        }

        public InterpolationViewModel(List<DataPoint> dataPoints, AbstractFunction function)
        {
            this.interpolationService= new NewtonInterpolation();
            this.dataPoints = dataPoints;
            this.function = function;
        }

        public InterpolationViewModel SetNewtonInterpolation()
        {
            this.interpolationService = new NewtonInterpolation();
            return this;
        }

        public List<DataPoint> Execute()
        {
            if (dataPoints != null)
            {
                try
                {
                    function.CheckADV(dataPoints);
                    interpolationService.Execute(dataPoints);
                    interpolationValues = interpolationService.GetResult();
                    StatusString = "";
                    return interpolationValues;
                }
                catch (Exception e)
                {
                    StatusString = e.Message;
                }
            }

            return new List<DataPoint>();
        }

        public List<DataPoint> GetFunctionValues()
        {
            var startArg  = dataPoints[0].X;
            var endArg = dataPoints[dataPoints.Count - 1].X;
            var interval = dataPoints.Count * 10;
            var step = (dataPoints[1].X - dataPoints[0].X)/(interval);
             
            var functionDataPoint = new List<DataPoint>();

            while (startArg < endArg)
            {
                functionDataPoint.Add(new  DataPoint(startArg, function.Function(startArg)));
                startArg += step;
            }

            functionDataPoint.Add(dataPoints[dataPoints.Count - 1]);

            return functionDataPoint;
        }
    }
}