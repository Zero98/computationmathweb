using System;
using System.Collections.Generic;
using WebComputationMath3.Models;
using WebComputationMath3.Services;
using WebComputationMath3.Services.DifferentialEquationServices;
using WebComputationMath3.Services.FunctionsTwoVariables;
using WebComputationMath3.Services.Interfaces;

namespace WebComputationMath3.ViewModels
{
    public class DifferentialEquationViewModel
    {
        private IInterpolationService interpolationService;

        private AbstractDifferentialEquation differentialEquationService;

        private AbstractFunction function;
        
        public string StatusString { get; private set; }

        public string FunctionName => $"y' = {function.Name}";

        public DifferentialEquationViewModel(string functionName)
        {
            function = FunctionManager.GetFunctionByName(functionName);
            this.interpolationService= new NewtonInterpolation();
            this.differentialEquationService = new MilneDifferentialEquation();
            
        }
        
        public DifferentialEquationViewModel SetNewtonInterpolation()
        {
            this.interpolationService = new NewtonInterpolation();
            return this;
        }

        public DifferentialEquationViewModel SetMilneDifferentialEquation()
        {
            this.differentialEquationService = new MilneDifferentialEquation();
            return this;
        }

        public List<DataPoint> TryExecute(DataPoint startPoint, double accuracy, double rightLimit)
        {
            try
            {
                var values = differentialEquationService.SolveEquation(startPoint, accuracy, rightLimit, function.Function);
                
                interpolationService.Execute(values);
                return interpolationService.GetResult();
            }
            catch (Exception e)
            {
                StatusString = e.Message;
            }
            return  new List<DataPoint>();
        }
    
        public string[] GetFunctionList() => FunctionManager.GetFunctionList();
    }
}