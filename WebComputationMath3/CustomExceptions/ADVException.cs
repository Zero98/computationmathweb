using System;

namespace WebComputationMath3.CustomExceptions
{
    public class ADVException : Exception
    {
        public ADVException(string message = "Значения не входят в ОДЗ!") : base(message) { }
    }
}